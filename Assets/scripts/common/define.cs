﻿using UnityEngine;
using System.Collections;

public class define {

	/// <summary>
	/// 对话框消失时间 单位秒
	/// </summary>
	public static float dialogDisapperaTime = 10f;

	/// <summary>
	/// 两次对话发送间隔
	/// </summary>
	public static float dialogDelayTime = 5f;

	/// <summary>
	/// 卡牌 高比宽比例
	/// </summary>
	public static float aspectRatio = 1.38f;

	/// <summary>
	/// 战场中展示对方使用卡牌时间
	/// </summary>
	public static float useCardDisappearTime = 3f;

	/// <summary>
	/// 加载资源所用目录
	/// 示例远程加载地址："http://www.jyyule.cn/game"
	/// 示例本地加载地址：@"file:///E:/项目/Assets/StreamingAssets/Actor.assetbundle"
	/// </summary>
	public static string baseURL = @"file:///D:/develop/game";
	//public static string baseURL = "http://www.jyyule.cn/game";

	public static string[] HeroCareer = {
		"法师",
		"猎人",
		"圣骑士",
		"战士",
		"德鲁伊",
		"术士",
		"萨满祭司",
		"牧师",
		"潜行者",
		"死亡骑士",
        "中立"
	};


	public static string[] HeroName = {
		"吉安娜·普罗德摩尔",
		"雷克萨",
		"乌瑟尔•光明使者",
		"加尔鲁什•地狱咆哮",
		"玛法里奥•怒风",
		"古尔丹",
		"萨尔",
		"安杜因•乌瑞恩",
		"瓦莉拉•萨古纳尔",
		"阿尔萨斯 ",
        "中立"
	};
}
