﻿using KBEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loginController : MonoBehaviour {

    public InputField id;
    public InputField password;

    public bool autoLogin = false;

    public void login()
    {
        Debug.LogFormat("尝试登陆：用户名：{0}  密码：{1}",id.text,password.text);
        KBEngine.Event.fireIn("login", id.text, password.text, System.Text.Encoding.UTF8.GetBytes("PC"));
    }

    public void onLoginFailed(UInt16 failedcode)
    {
        Debug.LogFormat("登陆失败，错误码{0}，原因：{1}",failedcode, KBEngineApp.app.serverErr(failedcode));
    }

    private void Start()
    {
        KBEngine.Event.registerOut("onLoginFailed", this, "onLoginFailed");
        KBEngine.Event.registerOut("onLoginSuccessfully", this, "onLoginSuccessfully");

        string path = Application.streamingAssetsPath;
        string idAdd = "2";
        if (path.Contains("2"))
        {

        }
        else
        {
            idAdd = "";
        }

        if (autoLogin)
        {
            Debug.LogFormat("尝试登陆：用户名：{0}  密码：{1}", "f" + idAdd, "ff");
            KBEngine.Event.fireIn("login", "f"+idAdd, "ff", System.Text.Encoding.UTF8.GetBytes("PC"));
        }
    }

    public void onLoginSuccessfully()
    {
        Debug.Log("登陆成功，即将跳转下一个场景");
        SceneManager.LoadScene(1);
    }

}
