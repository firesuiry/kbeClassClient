﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class kzDisplayManager : MonoBehaviour {

    public Image heroImage;
    public Text kzNameText;

    public kzCardDisplayManager displayManger;

    public static kzDisplayManager manager;

    private void Awake()
    {
        manager = this;
    }

    public void setKzCardDisplay(int heroIndex,string kzName,List<uint> cardList)
    {
        heroImage.sprite = common.getHeroSprint(heroIndex, "0");
        kzNameText.text = kzName;
        displayManger.displayCardList(cardList);
    }

    private void OnGUI()
    {if(Input.GetKey(KeyCode.G))
        setKzCardDisplay(3, "测试卡组", common.getCardIDList());
    }
}
