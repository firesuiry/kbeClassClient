﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KBEngine;
using UnityEngine.SceneManagement;

public class BFcontroller : MonoBehaviour {

    public clientEntity recvTargetObj;
    public int state = 0;
    //状态0 不是自己的回合 不能出牌
    //状态1 自己的回合 可以自由出牌
    //状态2 自己回合中 选择出牌的目标
    //状态3 自己的回合 选择攻击的目标

    public static BFcontroller manager;

    private void Awake()
    {
        manager = this;
    }

    void Start () {
        Debug.Log("请求切换控制实体");
        Account Me = KBEngineApp.app.player() as Account;
        if (Me != null)
        {
            Me.baseCall("reqEnterBattlefield");
        }

        KBEngine.Event.registerOut("battleEnd", this, "battleEnd");
    }

    public void getUseTarget(clientEntity e)
    {
        if(state != 1)
        {
            return;
        }

        state = 2;
        recvTargetObj = e;
    }

    public void getAttTarget(clientEntity e)
    {
        if (state != 1)
        {
            return;
        }

        state = 3;
        recvTargetObj = e;
    }

    public void onClickEntity(clientEntity e)
    {
        if(state == 2)
        {
            recvTargetObj.reqUse(e.id);
            state = 1;
        }
        else if(state == 3)
        {
            recvTargetObj.reqAtt(e.id);
            state = 1;
        }
        else if(state == 1)
        {
            getAttTarget(e);
        }
    }

    public void endRound()
    {
        if(state == 0)
        {
            return;
        }

        KBEngine.Avatar avatar = KBEngineApp.app.player() as KBEngine.Avatar;
        if(avatar != null)
        {
            avatar.cellCall("reqEndRound");
        }
    }

    public void giveUp()
    {

        KBEngine.Avatar avatar = KBEngineApp.app.player() as KBEngine.Avatar;
        if (avatar != null)
        {
            avatar.cellCall("reqGiveUp");
        }
    }

    public void battleEnd(object success)
    {
        Debug.LogFormat("战斗结束，战斗结果：【{0}】", success);
        Debug.Log("即将转换场景");

        SceneManager.LoadScene(1);

    }



}
