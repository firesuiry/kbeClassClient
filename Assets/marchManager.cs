﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KBEngine;

public class marchManager : MonoBehaviour {

    public Text timeText;
    public int choosedKzStore = -1;

    float startTime = 0f;

    public void endMarch()
    {
        Debug.Log("结束匹配");
        Account Me = KBEngineApp.app.player() as Account;
        if (Me != null)
        {
            Me.baseCall("reqStopMarch");
        }
        gameObject.SetActive(false);
        choosedKzStore = -1;
    }

    public void startMarch()
    {
        if (choosedKzStore == -1)
        {
            Debug.LogError("匹配失败，没有选择卡组");
            return;
        }
        Account Me = KBEngineApp.app.player() as Account;
        if (Me != null)
        {
            Me.baseCall("reqStartMarch",choosedKzStore);
        }
        choosedKzStore = -1;
    }

    private void OnEnable()
    {
        startTime = 0f;
        if(choosedKzStore == -1)
        {
            mainController.main.getKzChoose(getChoosedKz);
        }
    }

    private void OnDisable()
    {
        choosedKzStore = -1;
    }

    public void getChoosedKz(int index)
    {
        Debug.Log("getChoosedKz" + index.ToString());
        if(index == -1)
        {
            gameObject.SetActive(false);
            return;
        }

        startTime = Time.time;
        choosedKzStore = index;
        startMarch();
    }

    private void OnGUI()
    {
        if(startTime == 0f)
        {
            timeText.text = "即将开始匹配";
        }
        else
        {
            timeText.text = string.Format("当前已经匹配{0}秒", (int)(Time.time - startTime));

        }

    }

}
